/***
 * Interface for all random vector generators
 */
public interface RandomVectorGenerator {

	public double[] getVector();

}
