import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;

import static org.bridj.Pointer.allocateDoubles;
import static org.bridj.Pointer.allocateFloats;

/**
 * This class generates large vector of Gaussian random numbers from the input of
 * uniform random numbers.
 * Created by Songcy on 2014/12/9.
 */
public class GPUGaussianGenerator {

    public static double[] convert(double[] uniform) {
        final int N = uniform.length; // length of large vector
        double[] gaussian = new double[N];
        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
        CLDevice device = clPlatform.getBestDevice();
        CLContext context = JavaCL.createContext(null, device);
        CLQueue queue = context.createDefaultQueue();

        String src ="#pragma OPENCL EXTENSION cl_khr_fp64: enable \n" +
                "__kernel void convert_uniform_to_gaussian(__global double* a, __global double* b, int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n / 2)\n" +
                "        return;\n" +
                "\n" +
                "    b[2 * i] = sqrt(-2 * log(a[2 * i])) * cos(2 * 3.1416 * a[2 * i + 1]);\n" +
                "    b[2 * i + 1] = sqrt(-2 * log(a[2 * i])) * sin(2 * 3.1416 * a[2 * i + 1]);\n" +
                "}"; // Box–Muller transform
        CLProgram program = context.createProgram(src);
        program.build();

        CLKernel kernel = program.createKernel("convert_uniform_to_gaussian");

        final Pointer<Double>
                aPtr = allocateDoubles(N);

        for (int i = 0; i < N; i++) {
            aPtr.set(i, uniform[i]); // pointer of vector of uniform random numbers
        }

        CLBuffer<Double>
                a = context.createDoubleBuffer(CLMem.Usage.Input, aPtr);

        // Create an OpenCL output buffer :
        CLBuffer<Double> b = context.createDoubleBuffer(CLMem.Usage.Output, N);
        kernel.setArgs(a, b, N);
        CLEvent event = kernel.enqueueNDRange(queue, new int[]{N}, new int[]{128});
        final Pointer<Double> bPtr = b.read(queue, event);
        gaussian = bPtr.getDoubles();
        a.release(); //release the space so we have enough space next time
        b.release();
        aPtr.release();
        bPtr.release();
        return gaussian;
    }
}
