/***
 * Class for Asian Call Option.
 * @author Songcy
 */
public class AsianCallOption implements PayOut {

	private double K; // strike price

	public AsianCallOption(double K) {
		this.K = K;
	}

	/***
	 * The payout is max(0, the average price during the given period - K).
	 */
	@Override
	public double getPayout(double[] path) {
		return Math.max(0, getAverage(path) - K);
	}

	/***
	 * Calculate the average of a given array.
	 * 
	 * @param d
	 *            the given array
	 * @return average
	 */
	public static double getAverage(double[] d) {
		double sum = 0;
		for (int i = 0; i < d.length; i++) {
			sum += d[i];
		}
		return sum / d.length;
	}

}
