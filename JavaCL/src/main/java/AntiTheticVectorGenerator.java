import java.util.*;

/***
 * Decorator Pattern - AntiThetic. Suppose you use n1,...,nk to generate a path,
 * the next path is generated by -n1,...,-nk.
 * @author Songcy
 */
public class AntiTheticVectorGenerator implements RandomVectorGenerator {

	private RandomVectorGenerator rvg;
	double[] lastVector;

	public AntiTheticVectorGenerator(RandomVectorGenerator rvg) {
		this.rvg = rvg;
	}

	/***
	 * If lastVector is null, use rvg to generate a new random vector. Otherwise
	 * return the antithetic vector of lastVector.
	 */
	@Override
	public double[] getVector() {
		if (lastVector == null) {
			lastVector = rvg.getVector();
			return lastVector;
		} else {
			double[] tmp = Arrays.copyOf(lastVector, lastVector.length);
			lastVector = null;
			for (int i = 0; i < tmp.length; ++i) {
				tmp[i] = -tmp[i];
			}
			return tmp;
		}
	}

}
