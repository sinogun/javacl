/**
 * We do simulations in this class and print option prices and steps on console.
 * Created by Songcy on 2014/12/10.
 */
public class SimulationManager {

    public static void main(String[] args) {
        int step1 = 0;
        int step2 = 0;
        StatsCollector sc1 = new StatsCollector();
        StatsCollector sc2 = new StatsCollector();
        EuropeanCallOption ec = new EuropeanCallOption(165);
        AsianCallOption ac = new AsianCallOption(164);
        NormalRandomNumberGenerator nrg = new NormalRandomNumberGenerator(252);
        AntiTheticVectorGenerator avg = new AntiTheticVectorGenerator(nrg);
        while (!sc1.stop(2.055, 0.01)) { //simulation continues until meeting the criteria
            GBMRandomPathGenerator rpg = new GBMRandomPathGenerator(0.0001,
                    252, 0.01, 152.35, avg);
            sc1.add(ec.getPayout(rpg.getPath()));
            step1++;
        }
        System.out.println("The price of European call option is " + sc1.getAverage());
        System.out.println("Step of European call simulation: " + step1);
        while (!sc2.stop(2.055, 0.01)) { //simulation continues until meeting the criteria
            GBMRandomPathGenerator rpg = new GBMRandomPathGenerator(0.0001,
                    252, 0.01, 152.35, avg);
            sc2.add(ac.getPayout(rpg.getPath()));
            step2++;
        }
        System.out.println("The price of Asian call option is " + sc2.getAverage());
        System.out.println("Step of Asian call simulation: " + step2);
    }
}
