/***
 * This class generates a geometric Brownian motion path.
 * 
 * @author Songcy
 * 
 */
public class GBMRandomPathGenerator implements PathGenerator {

	private double rate; // interest rate
	private double sigma; // volatility
	private double S0; // spot price at time 0
	private int N; // number of observations
	private RandomVectorGenerator rvg; // random vector generator

	public GBMRandomPathGenerator(double rate, int N, double sigma, double S0,
			RandomVectorGenerator rvg) {
		this.rate = rate;
		this.S0 = S0;
		this.sigma = sigma;
		this.N = N;
		this.rvg = rvg;
	}

	/***
	 * Return a geometric Brownian motion path in a double array.
	 */
	@Override
	public double[] getPath() {
		double[] n = rvg.getVector();
		double[] path = new double[N];
		path[0] = S0;
		for (int i = 1; i < N; ++i) {
			path[i] = path[i - 1]
					* Math.exp((rate - sigma * sigma / 2) + sigma * n[i - 1]);
		}
		return path;
	}

}
