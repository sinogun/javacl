import java.util.*;

/***
 * Class where we do the statistic calculations and check if the simulation
 * should stop.
 * 
 * @author Songcy
 * 
 */
public class StatsCollector {

	ArrayList<Double> stats;
	double mean;
	double meansquared;
	double var;

	public StatsCollector() {
		stats = new ArrayList<Double>();
		mean = 0;
		meansquared = 0;
		var = 0;
	}

	/***
	 * Return the average of stats.
	 */
	public double getAverage() {
		return mean;
	}

	/***
	 * Return the mean of the square of all elements squared in stats.
	 */
	public double getMeanSquared() {
		return meansquared;
	}

	/***
	 * Return the variance of stats.
	 */
	public double getVar() {
		return var;
	}

	/***
	 * Update mean, meansquared and var.
	 */
	public void add(double d) {
		stats.add(d);
		int size = stats.size();
		if (size != 0) {
			mean = mean * (size - 1) / size + stats.get(size - 1) / size;
			meansquared = meansquared * (size - 1) / size + stats.get(size - 1)
					* stats.get(size - 1) / size;
			var = meansquared - mean * mean;
		}
	}

	/***
	 * If sqrt(var / size) * p < error, stop the simulation. Otherwise continue.
	 * 
	 * @param p
	 *            A number corresponding to the required probability. We need to
	 *            look it up in the standard normal distribution table.
	 * @param error
	 *            required maximum estimation error
	 * @return true for stop and false for continue
	 */
	public boolean stop(double p, double error) {
		if (stats.size() > 100) {
			return DoubleComparator.compare(Math.sqrt(var / stats.size()) * p,
					error, 0.00001) == -1;
		} else
			return false;
	}

}
