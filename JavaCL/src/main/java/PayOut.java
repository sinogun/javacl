/***
 * Interface for all kinds of options
 */
public interface PayOut {

	public double getPayout(double[] path);

}
