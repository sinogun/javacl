/***
 * Compare two double variables. This file is from the answer of summer
 * assignment uploaded by the professor.
 */

public class DoubleComparator {

	/***
	 * Check if two double variables are equal within tolerance.
	 * 
	 * @param d1
	 *            first double variable
	 * @param d2
	 *            second double variable
	 * @param tolerance
	 * @return true if equal and false if not
	 */
	public static boolean equal(double d1, double d2, double tolerance) {
		return Math.abs(d1 - d2) < tolerance;
	}

	/***
	 * Compare two double variables with a tolerance
	 * 
	 * @param d1
	 *            first double variable
	 * @param d2
	 *            second double variable
	 * @param tolerance
	 * @return 1 if d1 > d2 -1 if d1 < d2 0 if equal
	 */
	public static int compare(double d1, double d2, double tolerance) {
		double diff = d1 - d2;
		if (Math.abs(diff) < tolerance)
			return 0;
		return (int) Math.signum(diff);
	}

}
