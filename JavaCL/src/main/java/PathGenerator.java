/***
 * Interface for path generators
 */
public interface PathGenerator {

	public double[] getPath();

}
