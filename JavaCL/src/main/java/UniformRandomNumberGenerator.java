import java.util.Random;

/**
 * This class generates vector of random numbers uniformly distributed between 0 and 1.
 * Created by Songcy on 2014/12/9.
 */
public class UniformRandomNumberGenerator implements RandomVectorGenerator {

    private int N; // length of vector

    public UniformRandomNumberGenerator(int N) {
        this.N = N;
    }

    /***
     * Put uniformly distributed random numbers in an array.
     */
    @Override
    public double[] getVector() {
        double[] vector = new double[N];
        Random random = new Random();
        for (int i = 0; i < vector.length; i++) {
            vector[i] = random.nextDouble();
        }
        return vector;
    }
}
