/***
 * Class for European Call Option
 * @author Songcy
 */
public class EuropeanCallOption implements PayOut {

	private double K; // strike price

	public EuropeanCallOption(double K) {
		this.K = K;
	}

	/***
	 * The payout is max(0, price upon maturity - K).
	 */
	@Override
	public double getPayout(double[] path) {
		return Math.max(0, path[path.length - 1] - K);
	}

}
