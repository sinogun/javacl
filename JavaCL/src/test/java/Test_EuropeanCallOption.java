import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Test_EuropeanCallOption {

	EuropeanCallOption ec;
	double[] list;

	@Before
	public void setUp() throws Exception {
		list = new double[3];
		list[0] = 2.67;
		list[1] = 10.02;
		list[2] = 4.93;
		ec = new EuropeanCallOption(4.5);
	}

	@Test
	public void testGetPayout() {
		assertEquals(ec.getPayout(list), 0.43, 0.0001);
	}

}
