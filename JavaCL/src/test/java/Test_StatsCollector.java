import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Test_StatsCollector {

	StatsCollector sc;

	@Before
	public void setUp() throws Exception {
		sc = new StatsCollector();
	}

	@Test
	public void testGetAverage() {
		assertEquals(sc.getAverage(), 0, 0.001);
	}

	@Test
	public void testGetMeanSquared() {
		assertEquals(sc.getMeanSquared(), 0, 0.001);
	}

	@Test
	public void testGetVar() {
		assertEquals(sc.getVar(), 0, 0.001);
	}

	@Test
	public void testAdd() {
		sc.add(1);
		sc.add(2);
		sc.add(3);
		sc.add(4);
		assertEquals(sc.getAverage(), 2.5, 0.001);
		assertEquals(sc.getMeanSquared(), 7.5, 0.001);
		assertEquals(sc.getVar(), 1.25, 0.001);
	}

	@Test
	public void testStop() {
		assertFalse(sc.stop(2, 0.01));
		for (int i = 0; i < 1000; i++) {
			sc.add(2);
		}
		assertTrue(sc.stop(2, 0.01));
	}

}
