import static org.junit.Assert.*;

import org.junit.Test;

public class Test_DoubleComparator {

	@Test
	public void testEqual() {
		assertTrue(DoubleComparator.equal(0.01, 0.01, 0.0001));
		assertTrue(DoubleComparator.equal(0.01015, 0.01019, 0.0001));
	}

	@Test
	public void testCompare() {
		assertEquals(DoubleComparator.compare(0.01015, 0.01019, 0.0001), 0);
		assertEquals(DoubleComparator.compare(0.02, 0.01019, 0.0001), 1);
		assertEquals(DoubleComparator.compare(0.02, 0.03, 0.0001), -1);
	}

}
