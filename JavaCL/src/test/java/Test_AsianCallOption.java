import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Test_AsianCallOption {

	AsianCallOption ac;
	double[] list;

	@Before
	public void setUp() throws Exception {
		list = new double[3];
		list[0] = 2.67;
		list[1] = 10.02;
		list[2] = 4.93;
		ac = new AsianCallOption(4.5);
	}

	@Test
	public void testGetPayout() {
		assertEquals(ac.getPayout(list), 1.3733, 0.0001);
	}

	@Test
	public void testGetAverage() {
		assertEquals(
				AsianCallOption.getAverage(new double[] { 3.45, 6.98, 14.56 }),
				8.33, 0.0001);
	}

}
